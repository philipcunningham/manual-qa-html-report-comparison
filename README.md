## Manual QA - Gradle HTML Report

Project to compare Gemnasium Maven's output with Gradle's HTML Reports. Steps have been included below but you can simply browse the generated reports in their respective directories.

### Preamble

Build Gemnasium Maven locally so that the image matches your development machine's CPU architecture:

```bash
docker build -t gemnasium-maven:latest -f build/gemnasium-maven/debian/Dockerfile .;
```

### Base Case

Navigate to the relevant directory:

```bash
cd base-case
```

Run Gemnasium Maven:

```bash
docker run -it --rm -v "$PWD:/ci-project-dir" -e CI_PROJECT_DIR=/ci-project-dir -e DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER=true -w /ci-project-dir -e SECURE_LOG_LEVEL=debug gemnasium-maven:latest
```

Generate `htmlDependencyReport` manually:

```bash
docker run -it --rm -v "$PWD:/ci-project-dir" -w /ci-project-dir gemnasium-maven:latest bash
root@81d0c0ed3bbc:/ci-project-dir# gradle --init-script init.gradle htmlDependencyReport
```

Look at the SBOM from Gemnasium Maven:

```bash
cat gl-sbom-maven-gradle.cdx.json
```

Browse the HTML report in your browser:

```bash
open build/reports/project/dependencies/index.html
```

![base-base](base-case.png)

### Conflict Resolution Case

Navigate to the relevant directory:

```bash
cd conflict-resolution-case
```

Run the previous commands.

### Conflict Resolution Case with API Dependency

Navigate to the relevant directory:

```bash
cd conflict-resolution-with-api-dependency-case
```

Run Gemnasium Maven, setting the Java version:

```bash
docker run -it --rm -v "$PWD:/ci-project-dir" -e CI_PROJECT_DIR=/ci-project-dir -e DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER=true -w /ci-project-dir -e SECURE_LOG_LEVEL=debug -e DS_JAVA_VERSION=21 gemnasium-maven:latest
```

Generate `htmlDependencyReport` manually, setting the Gradle and Java versions:

```bash
docker run -it --rm -v "$PWD:/ci-project-dir" -w /ci-project-dir gemnasium-maven:latest bash
root@3574c649f570:/ci-project-dir# asdf global gradle 8.4asdf global gradle 8.4
root@3574c649f570:/ci-project-dir# asdf global java zulu-21.30.15
root@3574c649f570:/ci-project-dir# gradle --init-script init.gradle htmlDependencyReport
```

Browse the HTML reports and open the one that you're interested in:

```bash
find . -name "index.html"
./test/build/reports/project/dependencies/index.html
./opentelemetry/build/reports/project/dependencies/index.html
./archiver/build/reports/project/dependencies/index.html
./build/reports/project/dependencies/index.html
./rest/build/reports/project/dependencies/index.html
```

For example:

```bash
open build/reports/project/dependencies/index.html
```
