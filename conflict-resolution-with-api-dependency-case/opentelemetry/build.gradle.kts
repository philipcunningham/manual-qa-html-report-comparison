val openTelemetryKotlinVersion: String by project

dependencies {
    api("io.quarkus:quarkus-opentelemetry")
    implementation("io.opentelemetry:opentelemetry-extension-kotlin:$openTelemetryKotlinVersion")
}
