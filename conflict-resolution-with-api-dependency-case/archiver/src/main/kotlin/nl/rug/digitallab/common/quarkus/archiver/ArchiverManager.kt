package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import io.opentelemetry.instrumentation.annotations.SpanAttribute
import io.opentelemetry.instrumentation.annotations.WithSpan
import io.quarkus.arc.All
import jakarta.inject.Inject
import jakarta.inject.Singleton
import nl.rug.digitallab.common.quarkus.archiver.MimeTypeUtils.getMimeType
import nl.rug.digitallab.common.quarkus.archiver.exception.UnsupportedArchiveException
import org.jboss.logging.Logger
import java.nio.file.Path

/**
 * The ArchiveManager is a bean that can archive and unarchive files. It supports the following archive formats:
 * - TAR
 * - ZIP
 *
 * @property supportedArchivers The archive formats supported by this archive manager.
 */
@Singleton
class ArchiverManager {
    @Inject
    private lateinit var log: Logger

    @All
    lateinit var supportedArchivers: MutableList<Archiver>

    /**
     * Archive a directory to an archive file. The MIME type of the archive is determined by the file extension, but can
     * be overridden.
     *
     * @param sourceDirectory The directory to archive.
     * @param destinationArchive The archive file to create.
     * @param mimeType The MIME type of the archive. Defaults to the MIME type of the destination archive.
     *
     * @throws java.io.IOException If an I/O error occurs.
     */
    @WithSpan("ARCHIVE")
    fun archive(
        @SpanAttribute sourceDirectory: Path,
        @SpanAttribute destinationArchive: Path,
        mimeType: MediaType = destinationArchive.getMimeType(),
    ) {
        log.debug("Archiving $sourceDirectory to $destinationArchive")

        val archiver = supportedArchivers.find { it.supports(mimeType) }
            ?: throw UnsupportedArchiveException(mimeType)

        archiver.archive(sourceDirectory, destinationArchive)

        log.debug("Archived $sourceDirectory to $destinationArchive")
    }

    /**
     * Unarchive an archive file to a directory. The MIME type of the archive is determined by the file extension, but
     * can be overridden.
     *
     * @param sourceArchive The archive file to unarchive.
     * @param destinationDirectory The directory to unarchive to.
     * @param mimeType The MIME type of the archive. Defaults to the MIME type of the source archive.
     *
     * @throws java.io.IOException If an I/O error occurs.
     */
    @WithSpan("UNARCHIVE")
    fun unarchive(
        @SpanAttribute sourceArchive: Path,
        @SpanAttribute destinationDirectory: Path,
        mimeType: MediaType = sourceArchive.getMimeType(),
    ) {
        log.debug("Unarchiving $sourceArchive to $destinationDirectory")

        val unarchiver = supportedArchivers.find { it.supports(mimeType) }
            ?: throw UnsupportedArchiveException(mimeType)

        unarchiver.unarchive(sourceArchive, destinationDirectory)

        log.debug("Unarchived $sourceArchive to $destinationDirectory")
    }
}