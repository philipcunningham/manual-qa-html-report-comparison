package nl.rug.digitallab.common.quarkus.archiver

import com.kamelia.sprinkler.util.closeableScope
import jakarta.inject.Inject
import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveInputStream
import org.apache.commons.compress.archivers.ArchiveOutputStream
import org.jboss.logging.Logger
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.*

/**
 * Abstract class for archiving and unarchiving files using Apache Commons Compress.
 *
 * All Apache Commons Compress archive formats, except for 7z, use the same API.
 */
abstract class ApacheArchiver<T: ArchiveEntry>: Archiver() {
    @Inject
    protected lateinit var log: Logger

    @OptIn(ExperimentalPathApi::class)
    override fun archive(sourceDirectory: Path, destinationArchive: Path) {
        val fileOutputStream = destinationArchive.outputStream()
        val archiveOutputStream = createArchiveOutputStream(fileOutputStream)

        closeableScope(fileOutputStream, archiveOutputStream) {
            // Archive each file in the directory
            sourceDirectory.walk().forEach { path ->
                val relativePath = sourceDirectory.relativize(path)

                val entry = createArchiveEntry(path.toFile(), relativePath.toString())
                archiveOutputStream.putArchiveEntry(entry)

                log.debug("Archiving file ${path.absolute()} to archive entry ${entry.name}")

                // Copy file to archive entry
                path.inputStream().use { inputStream ->
                    inputStream.copyTo(archiveOutputStream)
                }

                archiveOutputStream.closeArchiveEntry()
            }
        }
    }

    override fun unarchive(sourceArchive: Path, destinationDirectory: Path) {
        val fileInputStream = sourceArchive.inputStream()
        val archiveInputStream = createArchiveInputStream(fileInputStream)

        closeableScope(fileInputStream, archiveInputStream) {
            while (true) {
                val entry = archiveInputStream.nextEntry ?: break

                // Check if entry can be read (archive is supported)
                if (!archiveInputStream.canReadEntryData(entry)) {
                    throw IOException("Cannot read archive entry data: ${entry.name}")
                }

                // If the archive entry is a directory, skip. Directories are created later.
                if (entry.isDirectory) {
                    continue
                }

                val outputFile = destinationDirectory.resolve(entry.name)
                log.debug("Unarchiving archive entry: ${entry.name} to ${outputFile.absolute()}")

                // Create parent directories and file
                Files.createDirectories(outputFile.parent)
                Files.createFile(outputFile)

                // Copy archive entry to file
                outputFile.outputStream().use { outputStream ->
                    archiveInputStream.copyTo(outputStream)
                }
            }
        }
    }

    /**
     * Create an [ArchiveEntry] for a file.
     *
     * @param file The file represented by the [ArchiveEntry].
     * @param fileName The name of the [ArchiveEntry].
     *
     * @return The [ArchiveEntry] for the file.
     */
    protected abstract fun createArchiveEntry(file: File, fileName: String): T

    /**
     * Create an [ArchiveOutputStream] from an [OutputStream].
     *
     * @param outputStream The [OutputStream] to create the [ArchiveOutputStream] from.
     *
     * @return The [ArchiveOutputStream] created from the [OutputStream].
     */
    protected abstract fun createArchiveOutputStream(outputStream: OutputStream): ArchiveOutputStream<T>

    /**
     * Create an [ArchiveInputStream] from an [InputStream].
     *
     * @param inputStream The [InputStream] to create the [ArchiveInputStream] from.
     *
     * @return The [ArchiveInputStream] created from the [InputStream].
     */
    protected abstract fun createArchiveInputStream(inputStream: InputStream): ArchiveInputStream<T>

}