package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import jakarta.inject.Singleton
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
import java.io.File
import java.io.InputStream
import java.io.OutputStream

/**
 * Archive and unarchive TAR files.
 */
@Singleton
class TarArchiver: ApacheArchiver<TarArchiveEntry>() {
    override val supportedMimeTypes: List<MediaType> = listOf(MediaType.TAR)

    override fun createArchiveEntry(file: File, fileName: String): TarArchiveEntry = TarArchiveEntry(file, fileName)
    override fun createArchiveInputStream(inputStream: InputStream): TarArchiveInputStream = TarArchiveInputStream(inputStream)
    override fun createArchiveOutputStream(outputStream: OutputStream): TarArchiveOutputStream = TarArchiveOutputStream(outputStream)
}