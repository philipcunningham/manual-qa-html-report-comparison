package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import jakarta.inject.Singleton
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream
import java.io.File
import java.io.InputStream
import java.io.OutputStream

/**
 * Archive and unarchive ZIP files.
 */
@Singleton
class ZipArchiver : ApacheArchiver<ZipArchiveEntry>() {
    override val supportedMimeTypes: List<MediaType> = listOf(MediaType.ZIP)

    override fun createArchiveEntry(file: File, fileName: String): ZipArchiveEntry = ZipArchiveEntry(file, fileName)
    override fun createArchiveInputStream(inputStream: InputStream): ZipArchiveInputStream = ZipArchiveInputStream(inputStream)
    override fun createArchiveOutputStream(outputStream: OutputStream): ZipArchiveOutputStream = ZipArchiveOutputStream(outputStream)
}