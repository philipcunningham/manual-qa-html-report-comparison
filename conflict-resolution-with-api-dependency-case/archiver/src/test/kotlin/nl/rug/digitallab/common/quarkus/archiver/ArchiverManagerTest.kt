package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import jakarta.inject.Inject
import nl.rug.digitallab.common.quarkus.archiver.exception.UnsupportedArchiveException
import nl.rug.digitallab.common.quarkus.test.StandaloneProfile
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.*

@QuarkusTest
@TestProfile(StandaloneProfile::class)
class ArchiverManagerTest {
    @Inject
    lateinit var archiverManager: ArchiverManager

    @Test
    fun `The number of supported archivers should be equal to the number of implementations of the Archiver interface`() {
        assertEquals(2, archiverManager.supportedArchivers.size)
    }

    @TestFactory
    @OptIn(ExperimentalPathApi::class)
    fun `All supported archive types should successfully archive and unarchive a directory`(): List<DynamicTest> {
        return archiverManager.supportedArchivers.map { archiver ->
            DynamicTest.dynamicTest("Archiver: ${archiver::class.simpleName}") {
                // Create a temporary archive file and unarchive directory
                val archiveFile = Files.createTempFile("archive-", "")
                val destinationDirectory = Files.createTempDirectory("unarchive-")

                // Archive and unarchive the example directory
                val dirPath = Paths.get("file", "example").toString()
                val sourceDirectory = loadResource(dirPath).toAbsolutePath()
                archiver.archive(sourceDirectory, archiveFile)
                archiver.unarchive(archiveFile, destinationDirectory)

                // List all files and directories in the source and destination directories
                val sourcePaths = sourceDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES)
                val destinationPaths = destinationDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES)

                val relativeSourcePaths = sourcePaths.map { sourceDirectory.relativize(it) }
                val relativeDestinationPaths = destinationPaths.map { destinationDirectory.relativize(it) }

                // Assert that the destination directory contains the same files and directories as the source directory
                assertEquals(relativeSourcePaths.toSet(), relativeDestinationPaths.toSet())

                val sourceContents = sourcePaths.filter { !it.isDirectory() }.map { it.readLines() }
                val destinationContents = destinationPaths.filter { !it.isDirectory() }.map { it.readLines() }

                // Assert that the content of all files in the destination directory is the same as the content of the source directory
                assertEquals(sourceContents.toSet(), destinationContents.toSet())
            }
        }
    }

    @Test
    @OptIn(ExperimentalPathApi::class)
    fun `Unarchiving a tar should result in the same files and directories present in the original tar`() {
        val tarFile = loadResource(Paths.get("file", "example.tar").toString())

        // Unarchive the tar file to a temporary directory
        val targetDirectory = Files.createTempDirectory("")
        archiverManager.unarchive(tarFile, targetDirectory)

        // List all files and directories in the unarchived tar
        val unarchivedDirectory = targetDirectory.resolve("example")
        val unarchivedPaths = unarchivedDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).map { unarchivedDirectory.relativize(it) }

        // List all files and directories in the original directory
        val dirPath = Paths.get("file", "example").toString()
        val originalDirectory = loadResource(dirPath).toAbsolutePath()
        val originalPaths = originalDirectory.walk(PathWalkOption.INCLUDE_DIRECTORIES).map { originalDirectory.relativize(it) }

        // Assert that the unarchived tar contains the same files and directories as the original directory
        assertEquals(6, unarchivedPaths.toList().count())
        assertEquals(originalPaths.toSet(), unarchivedPaths.toSet())

        // Assert that the content of all files in the unarchived tar is the same as the content of the original directory
        val unarchivedContents = unarchivedDirectory.walk().map { it.readLines() }
        val originalContents = originalDirectory.walk().map { it.readLines() }
        assertEquals(originalContents.toSet(), unarchivedContents.toSet())
    }

    @Test
    fun `Unarchiving an unsupported archive type should result in an UnsupportedArchiveException`() {
        val filePath = Paths.get("file", "example.tar.gz").toString()
        val unsupportedFile = loadResource(filePath)

        assertThrows<UnsupportedArchiveException> { archiverManager.unarchive(unsupportedFile, Files.createTempDirectory("")) }
    }

    @TestFactory
    fun `Archiving an unsupported MIME type should throw an UnsupportedArchiveException`(): List<DynamicTest> {
        return archiverManager.supportedArchivers.map { archiver ->
            DynamicTest.dynamicTest("Archiver: ${archiver::class.simpleName}") {
                val sourceDirectory = Files.createTempDirectory("source")
                val destinationArchive = Files.createTempFile("dest", ".unknown")
                val mimeType = MediaType.parse("application/unknown")

                assertThrows<UnsupportedArchiveException> {
                    archiverManager.archive(sourceDirectory, destinationArchive, mimeType)
                }

                Files.deleteIfExists(sourceDirectory)
                Files.deleteIfExists(destinationArchive)
            }
        }
    }

    @TestFactory
    fun `Archiving a supported MIME type should not throw exceptions`(): List<DynamicTest> {
        return archiverManager.supportedArchivers.map { archiver ->
            DynamicTest.dynamicTest("Archiver: ${archiver::class.simpleName}") {
                val sourceDirectory = Files.createTempDirectory("source")
                val destinationArchive = Files.createTempFile("dest", ".unknown")
                val mimeType = archiver.supportedMimeTypes.first()

                archiverManager.archive(sourceDirectory, destinationArchive, mimeType)

                Files.deleteIfExists(sourceDirectory)
                Files.deleteIfExists(destinationArchive)
            }
        }
    }

    @Test
    fun `Archiving a directory to an unsupported MIME type should throw an UnsupportedArchiveException`() {
        val sourceDirectory = Files.createTempDirectory("source")
        val destinationArchive = Files.createTempFile("dest", ".unknown")

        assertThrows<UnsupportedArchiveException>{
            archiverManager.archive(sourceDirectory, destinationArchive)
        }

        Files.deleteIfExists(sourceDirectory)
        Files.deleteIfExists(destinationArchive)
    }

    /**
     * Load a resource from the classpath (src/test/resources)
     *
     * @param path The path to the resource
     * @return The URI of the resource
     */
    private fun loadResource(path: String): Path {
        val resource = Thread.currentThread().contextClassLoader.getResource(path)
        requireNotNull(resource) { "Resource $path not found" }
        return resource.toURI().toPath()
    }
}