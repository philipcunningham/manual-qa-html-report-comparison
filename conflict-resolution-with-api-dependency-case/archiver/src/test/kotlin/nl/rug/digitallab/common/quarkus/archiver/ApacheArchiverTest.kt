package nl.rug.digitallab.common.quarkus.archiver

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import nl.rug.digitallab.common.quarkus.test.StandaloneProfile
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.toPath

@QuarkusTest
@TestProfile(StandaloneProfile::class)
class ApacheArchiverTest {
    @Test
    fun `The unarchive() function should throw an IOException when the ZIP is encrypted and not empty`() {
        val archiver = ZipArchiver() // Only ZIP supports encrypted archives

        val destinationDirectory = Files.createTempDirectory("dest")
        val filePath = Paths.get("file", "encryption", "encrypted.zip").toString()
        val archiveFile = loadResource(filePath).toAbsolutePath()

        assertThrows<IOException> {
            archiver.unarchive(archiveFile, destinationDirectory)
        }
    }

    private fun loadResource(path: String): Path {
        val resource = Thread.currentThread().contextClassLoader.getResource(path)
        requireNotNull(resource) { "Resource $path not found" }
        return resource.toURI().toPath()
    }
}