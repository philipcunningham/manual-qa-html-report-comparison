package nl.rug.digitallab.common.quarkus.archiver

import com.google.common.net.MediaType
import nl.rug.digitallab.common.quarkus.archiver.MimeTypeUtils.getMimeType
import nl.rug.digitallab.common.quarkus.archiver.exception.UnknownMimeTypeException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.nio.file.Files
import java.nio.file.Path

class MimeTypeUtilsTest {

    @Test
    fun `getMimeType should return correct MIME type for known extension`() {
        val tempFile = Files.createTempFile(null, ".txt")
        val expectedMimeType = MediaType.parse("text/plain")

        val mimeType = tempFile.getMimeType()

        assertEquals(expectedMimeType, mimeType)
        Files.deleteIfExists(tempFile)
    }

    @Test
    fun `getMimeType should return default MIME type for unknown extension`() {
        val tempFile = Files.createTempFile(null, ".xyz")
        val expectedMimeType = MediaType.OCTET_STREAM

        val mimeType = tempFile.getMimeType()

        assertEquals(expectedMimeType, mimeType)
        Files.deleteIfExists(tempFile)
    }

    @Test
    fun `getMimeType should throw UnknownMimeTypeException for directory`() {
        val tempDirectory = Files.createTempDirectory(null)

        assertThrows<UnknownMimeTypeException> {
            tempDirectory.getMimeType()
        }

        Files.deleteIfExists(tempDirectory)
    }
}
