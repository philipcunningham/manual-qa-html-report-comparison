package nl.rug.digitallab.common.quarkus.archiver.exception

import com.google.common.net.MediaType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.nio.file.Paths

class ExceptionTest {

    @Test
    fun `UnknownMimeTypeException should have correct message`() {
        val testPath = Paths.get( "file", "rootfile.txt")
        val expectedMessage = "Unknown MIME type for file '$testPath'"

        val exception = assertThrows<UnknownMimeTypeException> {
            throw UnknownMimeTypeException(testPath)
        }

        assertEquals(expectedMessage, exception.message)
    }

    @Test
    fun `UnsupportedArchiveException should have correct message`() {
        val testMimeType: MediaType = MediaType.ZIP
        val expectedMessage = "Unsupported archive type: $testMimeType"

        val exception = assertThrows<UnsupportedArchiveException> {
            throw UnsupportedArchiveException(testMimeType)
        }

        assertEquals(expectedMessage, exception.message)
    }
}
