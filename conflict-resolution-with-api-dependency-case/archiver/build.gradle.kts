val commonsCompressVersion: String by project
val guavaVersion: String by project
val sprinklerUtilsVersion: String by project

dependencies {
    api("io.quarkus:quarkus-opentelemetry")
    api("com.google.guava:guava:$guavaVersion")
    api("org.apache.commons:commons-compress:$commonsCompressVersion")

    implementation("com.black-kamelia.sprinkler:utils:$sprinklerUtilsVersion") // closeableScope
}
