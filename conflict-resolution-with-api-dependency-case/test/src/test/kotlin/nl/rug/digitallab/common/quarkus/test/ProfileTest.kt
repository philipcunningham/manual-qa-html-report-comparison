package nl.rug.digitallab.common.quarkus.test

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class ProfileTest {
    
    @Test
    fun `ExternalDependenciesProfile should have correct tags`() {
        val externalDependenciesProfile = ExternalDependenciesProfile()
        Assertions.assertEquals(externalDependenciesProfile.tags(), mutableSetOf("external-dependencies"))
    }

    @Test
    fun `StandaloneProfile should have correct tags`() {
        val standaloneProfile = StandaloneProfile()
        Assertions.assertEquals(standaloneProfile.tags(), mutableSetOf("standalone"))
    }
}