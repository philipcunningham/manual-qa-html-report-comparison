package nl.rug.digitallab.common.quarkus.test

import io.quarkus.test.junit.QuarkusTestProfile

/**
 * A Quarkus test profile that can be used to run tests that depend on external services.
 *
 * Run with: test -Dquarkus.test.profile.tags=external-dependencies
 */
open class ExternalDependenciesProfile: QuarkusTestProfile {
    override fun tags(): MutableSet<String> = mutableSetOf("external-dependencies")
}
