package nl.rug.digitallab.common.quarkus.test

import io.quarkus.test.junit.QuarkusTestProfile

/**
 * A Quarkus test profile that can be used to run tests that are standalone and do not
 * require any external dependencies to execute.
 *
 * Run with: test -Dquarkus.test.profile.tags=standalone
 */
open class StandaloneProfile: QuarkusTestProfile {
    override fun tags(): MutableSet<String> = mutableSetOf("standalone")
}
