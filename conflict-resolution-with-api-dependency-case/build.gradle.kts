val projectGroupId: String by project

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

allprojects {
    group = projectGroupId
}

subprojects {
    apply {
        plugin("nl.rug.digitallab.gradle.plugin.quarkus.library")
    }

    if(project.name == "test") {
        configurations {
            testImplementation {
                exclude("nl.rug.digitallab.common.quarkus", "test")
            }
        }
    } else {
        dependencies {
            testImplementation(project(":test"))
        }
    }
}

// Prevents publishing the root project to Maven but allows publishing subprojects from the root project
tasks.withType(PublishToMavenRepository::class) {
    enabled = false
}
