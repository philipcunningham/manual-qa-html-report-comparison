rootProject.name = "common-quarkus"

pluginManagement {
    val digitalLabGradlePluginVersion: String by settings
    plugins {
        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Maven
        gradlePluginPortal()
    }
}

include("archiver")
include("opentelemetry")
include("rest")
include("test")
