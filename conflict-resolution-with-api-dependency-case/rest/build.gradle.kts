val mockitoKotlinVersion: String by project

dependencies {
    api("io.quarkus:quarkus-resteasy-reactive")
    api("io.quarkus:quarkus-resteasy-reactive-jackson")

    implementation("io.quarkus:quarkus-opentelemetry")

    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion")
}
