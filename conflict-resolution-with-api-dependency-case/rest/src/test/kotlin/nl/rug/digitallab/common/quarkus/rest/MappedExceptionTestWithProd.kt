package nl.rug.digitallab.common.quarkus.rest

import io.quarkus.runtime.LaunchMode
import io.quarkus.runtime.configuration.ProfileManager
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.TestProfile
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import nl.rug.digitallab.common.quarkus.test.StandaloneProfile
import org.hamcrest.CoreMatchers.*
import org.jboss.resteasy.reactive.RestResponse.Status
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test


@QuarkusTest
@TestProfile(StandaloneProfile::class)
class MappedExceptionTestWithProd {
    companion object {
        @JvmStatic
        @BeforeAll
        fun beforeAll() = ProfileManager.setLaunchMode(LaunchMode.NORMAL)

        @JvmStatic
        @AfterAll
        fun afterAll() = ProfileManager.setLaunchMode(LaunchMode.TEST)
    }

    @Test
    fun `MappedExceptions in NORMAL launch mode should not include a stack trace`() {
        When {
            get("/test-resource/bad-request-mapped-exception-endpoint")
        } Then {
            statusCode(Status.BAD_REQUEST.statusCode)
            body("errorMessage", equalTo("BadRequestMappedException"))
            body("errorId", notNullValue())
            body("stackTrace", nullValue())
        }
    }
}