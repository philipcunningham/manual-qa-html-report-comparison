package nl.rug.digitallab.common.quarkus.rest

import jakarta.ws.rs.*
import jakarta.ws.rs.core.Response
import org.jboss.resteasy.reactive.RestResponse.Status


@Path("test-resource")
class TestResource {
    class BadRequestMappedException: MappedException("BadRequestMappedException", Status.BAD_REQUEST)
    class NotFoundMappedException: MappedException("NotFoundMappedException", Status.NOT_FOUND)
    class PaymentRequiredMappedException: MappedException("PaymentRequiredMappedException", Status.PAYMENT_REQUIRED)
    class NormalException: Exception("NormalException")

    @GET
    @Path("bad-request-mapped-exception-endpoint")
    @Produces("application/json")
    fun badRequestMappedExceptionEndpoint(): String {
        throw BadRequestMappedException()
    }

    @GET
    @Path("not-found-mapped-exception-endpoint")
    @Produces("application/json")
    fun notFoundMappedExceptionEndpoint(): String {
        throw NotFoundMappedException()
    }

    @GET
    @Path("payment-required-mapped-exception-endpoint")
    @Produces("application/json")
    fun paymentRequiredMappedExceptionEndpoint(): String {
        throw PaymentRequiredMappedException()
    }

    @GET
    @Path("normal-exception-endpoint")
    @Produces("application/json")
    fun normalExceptionEndpoint(): String {
        throw NormalException()
    }

    @POST
    @Path("post-json-endpoint")
    @Consumes("application/json")
    @Produces("application/json")
    fun postJsonEndpoint(): Pair<String, String> = "message" to "Hello world"

    @GET
    @Path("not-found-exception")
    @Produces("application/json")
    fun notFoundExceptionEndpoint(): Response {
        throw NotFoundException()
    }
}