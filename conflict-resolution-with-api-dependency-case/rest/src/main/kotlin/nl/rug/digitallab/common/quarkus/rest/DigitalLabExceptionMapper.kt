package nl.rug.digitallab.common.quarkus.rest

import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.instrumentation.api.instrumenter.LocalRootSpan
import io.quarkus.runtime.LaunchMode
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.NotFoundException
import jakarta.ws.rs.NotSupportedException
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.RestResponse
import org.jboss.resteasy.reactive.server.ServerExceptionMapper

/**
 * The Digital Lab exception mapper intercepts selected exceptions thrown by the application and maps them to a
 * [RestResponse] with an [ErrorResponse] (JSON) as body. The goal of the Digital Lab exception mapper is to standardize
 * the error responses of the Digital Lab services. The error response contains an errorId that can be used to trace the
 * error in the logs. The errorId is also added to the OTel span. The error response also contains a stack trace, but
 * only when the application is not running in production mode, to prevent leaking internal information.
 *
 * The Digital Lab exception mapper is automatically registered by Quarkus.
 */
@ApplicationScoped
class DigitalLabExceptionMapper {
    @Inject
    private lateinit var log: Logger

    @ServerExceptionMapper
    fun mapException(e: MappedException): RestResponse<ErrorResponse> {
        return handleException(e, e.message, e.httpStatusCode)
    }

    @ServerExceptionMapper
    fun mapException(e: NotSupportedException): RestResponse<ErrorResponse> {
        // Override the default mapper for NotSupportedException
        return handleException(e, e.message, RestResponse.Status.UNSUPPORTED_MEDIA_TYPE)
    }

    @ServerExceptionMapper
    fun mapException(e: NotFoundException): RestResponse<ErrorResponse> {
        // Override the default mapper for NotFoundException
        return handleException(e, e.message, RestResponse.Status.NOT_FOUND)
    }

    @ServerExceptionMapper
    fun mapException(e: Exception): RestResponse<ErrorResponse> {
        // Exceptions without a specific mapper are mapped to a 500 error with a default message to prevent leaking internal information
        return handleException(e, "An internal server error occurred", RestResponse.Status.INTERNAL_SERVER_ERROR)
    }

    /**
     * Handles an exception by logging it, creating an [ErrorResponse] JSON body, and adding the errorId to the OTel span.
     *
     * @param e The exception to handle.
     * @param errorMessage The error message to include in the response to the client, this may differ from the
     * [Exception] message.
     * @param httpStatusCode The HTTP status code to include in the response to the client.
     *
     * @return A [RestResponse] with an [ErrorResponse] (JSON) as body.
     */
    private fun handleException(e: Exception, errorMessage: String?, httpStatusCode: RestResponse.Status): RestResponse<ErrorResponse> {
        log.error("An exception occurred:${System.lineSeparator()}", e)

        val errorResponse = ErrorResponse(errorMessage ?: "An unknown error occurred")

        // Set the status and errorId in the OTel span
        LocalRootSpan
            .current()
            .setStatus(StatusCode.ERROR)
            .setAttribute("errorId", errorResponse.errorId.toString())

        // If the application is not running in production mode, include the stack trace in the response
        if(LaunchMode.current() != LaunchMode.NORMAL) {
            errorResponse.stackTrace = e.stackTraceToString()
        }

        return RestResponse.status(httpStatusCode, errorResponse)
    }
}
