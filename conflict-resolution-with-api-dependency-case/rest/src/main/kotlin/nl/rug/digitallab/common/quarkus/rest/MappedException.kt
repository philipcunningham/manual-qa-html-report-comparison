package nl.rug.digitallab.common.quarkus.rest

import org.jboss.resteasy.reactive.RestResponse.Status

/**
 * Represents an exception that can be mapped to a specific HTTP status code. These exceptions are automatically mapped
 * by the [DigitalLabExceptionMapper].
 */
open class MappedException(message: String, val httpStatusCode: Status): Exception(message)
